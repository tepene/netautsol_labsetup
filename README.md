# NetAutSol Lab Setup

This project describes my lab setup for the autumn 2019 edition of the [Building Network Automation Solutions](https://www.ipspace.net/Building_Network_Automation_Solutions) course.

## Introduction

The lab setup is built to run on a single computer but you could easily distrubute the components on different systems.

![Lab Overview](img/overview.png)

A container with ansible installed runs on LXD. The LXD container is connected to the linux host with a network bridge (lxdbr0). The network equipment is emulated on a EVE-NG installation which is running on VMWare Workstation. The mangement interface of the EVE-NG instance (pnet0) is reachable behind a NAT network configured on vmnet8 on the linux host.

## Required Software

The project setup has been tested on Ubuntu 18.04.3 LTS. Other distros will most probably work as well but might need some tweaking.

The following software is required to build the lab:

- [LXD](https://help.ubuntu.com/lts/serverguide/lxd.html)
- [VMware Workstation](https://www.vmware.com/products/workstation-pro/workstation-pro-evaluation.html)
- [EVE-NG](https://www.eve-ng.net/)
- [Vagrant](https://www.vagrantup.com/downloads.html)
- [Vagrant-LXD Plugin](https://gitlab.com/catalyst-it/devtools/vagrant-lxd)

There are plenty of installation instruction out there, so I'm not going to describe them in detail here.

## Configruation

After the basic software installation the following configurations must be made to get the lab running.

### Assumption

The following assumptions are made:

1. The name of EVE-NG VM on VMware workstation must be set to **eve-ng**
2. The default location for virtual machines on VMware workstation must be named **vmware** and it must be located in the parent directory of your lab folder.

   __Example:__ When you configure your lab under **/home/stephan/virtual-labs/NetAutSol** the root folder must be under **/home/stephan/virtual-labs/vmware**

3. You have the [NetOpsWorkshop](https://github.com/ipspace/NetOpsWorkshop) repository downloaded to your lab folder

### VMware Workstation

Create a new network in the Virtual Network Editor of VMware workstation.

#### vmnet8

The vmnet8 network is configured with NAT. This network is used to get management access to the network interface (pnet0) of EVE-NG.

![vment8](img/vmnet8_1.png) ![vmnet](img/vmnet8_2.png)

The vmnet8 interface must be configured in promiscuous mode. This will be done automatically when you start the EVE-NG instance. But the configuration will only happen if your users has enough permission to do so. Therefore you must the change the permission for the Virtual Ethernet Adapter. This can be done with the following command:

```bash
sudo chmod a+rw /dev/vmnet8
```

__Source:__ [Using Virtual Ethernet Adapters in Promiscuous Mode on a Linux Host](https://kb.vmware.com/s/article/287)

#### Virtual Machine Settings

The virtual networks must be connected to the correct interfaces of the EVE-NG installation

- vmnet8 must be connected to the first adapter
- the second adapter can be set to "Host-only"

![network adapters](img/network-adapter.png)

### EVE-NG

#### Management Access

The pnet0 interface must be configured with the desired IP address for management access.

```bash
4: pnet0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether 00:0c:29:63:d6:4b brd ff:ff:ff:ff:ff:ff
    inet 192.168.126.10/24 brd 192.168.126.255 scope global pnet0
       valid_lft forever preferred_lft forever
    inet6 fe80::20c:29ff:fe63:d64b/64 scope link 
       valid_lft forever preferred_lft forever
```

#### Lab Topology

![topology](img/topology.png)

Tthe lab topology is deliberately kept simple. To get started you can import my ![topology export](eve-ng/NetAutSol.zip) and load the startup-configuration files provided in the ![eve-ng](eve-ng) folder or you can build your own lab.

### Vagrant

The **VAGRANT** configuration file is set up to launch a Ubuntu 18.04 image on LXD. Currently I am using a custom image which is not available in the [Vagrant Cloud](https://app.vagrantup.com/boxes/search). You must therefore specify your own image in the configuration file.

```ruby
    config.vm.define "ansible" do |ansible|
        ansible.vm.box = 'tepene/ubuntu-18.04'
```

## Usage

### Start ansible and EVE-NG

To start the lab you can simply run

```bash
vagrant up
```

This will launch the VM with the name **eve-ng** on VMware workstation and build the lxd container **ansible**. Once the ansible container is started, the OS will be updated and an installation script will be run to install ansible and ansible-napalm in a python virtual environment under **/opt/ansible**.

### Stop ansible and EVE-NG

To stop EVE-NG and ansible container you can run

```bash
vagrant halt
```
