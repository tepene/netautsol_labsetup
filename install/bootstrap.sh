#!/bin/bash

# Create link for set_permission.sh which will be executed on every login
echo "Creating link to customize.sh"
sudo ln -s /vagrant/install/customize.sh /etc/profile.d/customize.sh
sudo chmod +x /vagrant/install/customize.sh

# Wait for auto update to finish
echo "Waiting for auto update service to finish"
systemd-run --property="After=apt-daily.service apt-daily-upgrade.service" --wait /bin/true

# Install Ansible/NAPALM 
echo "Starting Ansible installation"
/vagrant/install/ansible.sh
