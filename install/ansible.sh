#!/bin/bash

cat <<EOM
Ansible installation script
===========================
This script updates your system, installs ansible and other packages

The script was adapted for and tested on Ubuntu 18.04. Do not use it
on earlier versions of Ubuntu.

===========================
EOM

QUIET="-y -qq"

# Upgrade System
echo "Upgrading base installation"
sudo apt-get $QUIET update && sudo apt-get $QUIET upgrade

# Install Python virtualenv
sudo apt-get $QUIET install virtualenv

# Create virtualenv
echo "Creating virtual environment for ansible installation"
sudo virtualenv -p python3 /opt/ansible
sudo chown -R vagrant:root /opt/ansible

# Install ansible and addition modules
echo "Installing ansible and additional modules"
source /opt/ansible/bin/activate
pip install -q ansible napalm-ansible

# Verify Installation
echo "Verifying ansible installation"
ansible-playbook --version
echo "Verifying napalm-ansible installation"
napalm-ansible

# Reboot
echo "Rebooting container"
sudo reboot