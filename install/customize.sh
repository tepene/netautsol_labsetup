#!/bin/bash

# Fix permission on home folder for user vagrant
chown -R vagrant:vagrant /home/vagrant

# Load python virtual environment to run ansible when user vagrant logs in
if [ ! -f /opt/ansible/.customized ]; then
    echo "" >> /home/vagrant/.bashrc
    sed -i '$ a # activate python virtual environment for ansible' /home/vagrant/.bashrc
    sed -i '$ a source /opt/ansible/bin/activate' /home/vagrant/.bashrc   
    touch /opt/ansible/.customized
fi
